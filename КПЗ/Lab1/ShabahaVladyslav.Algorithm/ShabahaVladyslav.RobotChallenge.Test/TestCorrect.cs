﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace ShabahaVladyslav.RobotChallenge.Test
{
    [TestClass]
    public class TestCorrect
    {
        [TestMethod]
        public void TestMethod1()
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(11, 11), RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(1, 1), Owner = new Owner(){Name = "Shabaha V."}}
            };
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(11, 10), Owner = new Owner() { Name = "Shabaha V." } });
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(12, 10), Owner = new Owner() { Name = "Shabaha V." } });
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(12, 11), Owner = new Owner() { Name = "Shabaha V." } });
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(12, 12), Owner = new Owner() { Name = "Shabaha V." } });
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(11, 12), Owner = new Owner() { Name = "Shabaha V." } });
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(10, 12), Owner = new Owner() { Name = "Shabaha V." } });
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(10, 11), Owner = new Owner() { Name = "Shabaha V." } });
            //robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(3, 3), Owner = new Owner() { Name = "Shabaha V." } });
            

            Position test = Correct(map.Stations[0], robots[0], robots).Position;
            
            Assert.AreEqual(test.X, 10);
        }

        public static EnergyStation Correct(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = station;
            var best = FindDistance(station.Position, movingRobot.Position);
            EnergyStation BestPos = station;
            nearest.Position.Y += 1;
            if (FindDistance(nearest.Position, movingRobot.Position) < best && IsCellFree(nearest, movingRobot, robots))
            {
                BestPos = nearest;
                best = FindDistance(BestPos.Position, movingRobot.Position);
                return BestPos;
            }
            nearest.Position.X += 1;
            if (FindDistance(nearest.Position, movingRobot.Position) < best && IsCellFree(nearest, movingRobot, robots))
            {
                BestPos.Position = nearest.Position;
                best = FindDistance(BestPos.Position, movingRobot.Position);
                return BestPos;
            }
            nearest.Position.X -= 2;
            if (FindDistance(nearest.Position, movingRobot.Position) < best && IsCellFree(nearest, movingRobot, robots))
            {
                BestPos.Position = nearest.Position;
                best = FindDistance(BestPos.Position, movingRobot.Position);
                return BestPos;
            }
            nearest.Position.Y -= 1;
            if (FindDistance(nearest.Position, movingRobot.Position) < best && IsCellFree(nearest, movingRobot, robots))
            {
                BestPos.Position = nearest.Position;
                best = FindDistance(BestPos.Position, movingRobot.Position);
                return BestPos;
            }
            nearest.Position.Y -= 1;
            if (FindDistance(nearest.Position, movingRobot.Position) < best && IsCellFree(nearest, movingRobot, robots))
            {
                BestPos.Position = nearest.Position;
                best = FindDistance(BestPos.Position, movingRobot.Position);
                return BestPos;
            }
            nearest.Position.X += 1;
            if (FindDistance(nearest.Position, movingRobot.Position) < best && IsCellFree(nearest, movingRobot, robots))
            {
                BestPos.Position = nearest.Position;
                best = FindDistance(BestPos.Position, movingRobot.Position);
                return BestPos;
            }
            nearest.Position.X += 1;
            if (FindDistance(nearest.Position, movingRobot.Position) < best && IsCellFree(nearest, movingRobot, robots))
            {
                BestPos.Position = nearest.Position;
                best = FindDistance(BestPos.Position, movingRobot.Position);
                return BestPos;
            }
            nearest.Position.X += 1;
            if (FindDistance(nearest.Position, movingRobot.Position) < best && IsCellFree(nearest, movingRobot, robots))
            {
                BestPos.Position = nearest.Position;
                best = FindDistance(BestPos.Position, movingRobot.Position);
                return BestPos;
            }
            nearest.Position.Y += 1;
            if (FindDistance(nearest.Position, movingRobot.Position) < best && IsCellFree(nearest, movingRobot, robots))
            {
                BestPos.Position = nearest.Position;
                best = FindDistance(BestPos.Position, movingRobot.Position);
                return BestPos;
            }
            return BestPos;
        }

        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(b.X - a.X, 2) + Math.Pow(b.Y - a.Y, 2));
        }

        public static bool IsCellFree(EnergyStation Station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot.Position == Station.Position && (robot.Energy * 0.05 - 50) < 0)
                    return false;
            }
            return true;
        }
    }
}
