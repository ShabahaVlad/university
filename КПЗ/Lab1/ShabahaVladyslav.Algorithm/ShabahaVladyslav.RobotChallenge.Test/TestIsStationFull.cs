﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace ShabahaVladyslav.RobotChallenge.Test
{
    [TestClass]
    public class TestIsStationFull
    {
        [TestMethod]
        public void TestMethod1()
        {
            var map = new Map();
            var stationPosition = new Position(11, 11);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            foreach (var station in map.Stations)
            {
                Assert.IsTrue(IsStationFull(station, 999));
            }
        }

        public static bool IsStationFull(EnergyStation station, int full)
        {
            if (station.Energy > full)
                return true;
            return false;
        }
    }
}
