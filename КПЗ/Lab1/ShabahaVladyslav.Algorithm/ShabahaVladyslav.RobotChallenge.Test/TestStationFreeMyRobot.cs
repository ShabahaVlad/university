﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace ShabahaVladyslav.RobotChallenge.Test
{
    [TestClass]
    public class TestStationFreeMyRobot
    {
        [TestMethod]
        public void TestMethod1()
        {
            var map = new Map();
            var stationPosition = new Position(11, 11);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(11, 11), Owner = new Owner(){Name = "Shabaha V."}}
            };
            foreach(var station in map.Stations)
                Assert.IsFalse(IsStationFreeMyRobot(robots, station.Position));
        }

        public static bool IsStationFreeMyRobot(IList<Robot.Common.Robot> robots, Position stationPosition)
        {
            foreach (var robot in robots)
            {
                if (robot.Owner.Name == "Shabaha V." && robot.Position == stationPosition)
                    return false;
            }
            return true;
        }
    }
}
