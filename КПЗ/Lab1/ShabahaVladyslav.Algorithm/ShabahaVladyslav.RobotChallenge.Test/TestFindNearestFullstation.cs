﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using ShabahaVladyslav.Algorithm;

namespace ShabahaVladyslav.RobotChallenge.Test
{
    [TestClass]
    public class TestFindNearestFullStation
    {
        [TestMethod]
        public void TestMethod1()
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            var map = new Map();
            var stationPosition = new Position(11, 11);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(11, 11), Owner = new Owner(){Name = "Shabaha V."}}
            };
            foreach (var station in map.Stations)
            {
                if (IsStationFull(station, 40) && IsStationFreeMyRobot(robots, station.Position))
                {
                    Assert.IsTrue(IsStationFreeMyRobot(robots, station.Position));
                }
                Assert.IsFalse(IsStationFreeMyRobot(robots, station.Position));
            }
        }

        public static bool IsStationFull(EnergyStation station, int full)
        {
            if (station.Energy > full)
                return true;
            return false;
        }

        public static bool IsStationFreeMyRobot(IList<Robot.Common.Robot> robots, Position stationPosition)
        {
            foreach (var robot in robots)
            {
                if (robot.Owner.Name == "Shabaha V." && robot.Position == stationPosition)
                    return false;
            }
            return true;
        }
    }
}
