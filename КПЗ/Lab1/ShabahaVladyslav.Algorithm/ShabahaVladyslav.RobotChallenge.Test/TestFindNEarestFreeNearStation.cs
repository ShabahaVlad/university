﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using ShabahaVladyslav.Algorithm;

namespace ShabahaVladyslav.RobotChallenge.Test
{
    [TestClass]
    public class TestFindNEarestFreeNearStation
    {
        [TestMethod]
        public void TestMethod1()
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            var map = new Map();
            var stationPosition = new Position(11, 11);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(3,3), RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(1, 1), Owner = new Owner(){Name = "Shabaha V."}}
            };
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(3, 3), Owner = new Owner() { Name = "Shabaha V." } });
            Position test = null;
            foreach (var robot in robots)
                test = FindNearestFreeSellNearStation(robot, map, robots);
            Assert.AreEqual(test.X, 11);
        }

        public static Position FindNearestFreeSellNearStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            IList<EnergyStation> listStation = new List<EnergyStation>();
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);

                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                        listStation.Clear();
                        listStation.Add(station);
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }

        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }

        public static bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot.Position == cell)
                    return false;
            }
            return true;
        }
    }
}
