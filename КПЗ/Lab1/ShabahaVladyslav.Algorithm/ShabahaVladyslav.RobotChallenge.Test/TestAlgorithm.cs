﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace ShabahaVladyslav.RobotChallenge.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestMethod1()
        {
            var algorithm = new Algorithm.ShabahaVladyslav();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation(){Energy = 1000, Position = stationPosition, RecoveryRate = 2});
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(1, 1)}
            };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
            //Assert.AreEqual(((MoveCommand) command).NewPosition, stationPosition);
        }
    }
}
