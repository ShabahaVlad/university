﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace ShabahaVladyslav.RobotChallenge.Test
{
    [TestClass]
    public class TestFreeCell
    {
        [TestMethod]
        public void TestMethod1()
        {
            var map = new Map();
            var stationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var nearest = map.Stations.First();
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(1, 1)}
            };
            var movingRobot = robots.First();
            nearest.Position.Y += 1;
            if (IsCellFree(nearest.Position, movingRobot, robots))
                IsTrue(movingRobot.Position, nearest.Position);
            nearest.Position.X += 1;
            if (IsCellFree(nearest.Position, movingRobot, robots))
                IsTrue(movingRobot.Position, nearest.Position);
            nearest.Position.X -= 2;
            if (IsCellFree(nearest.Position, movingRobot, robots))
                IsTrue(movingRobot.Position, nearest.Position);
            nearest.Position.Y -= 1;
            if (IsCellFree(nearest.Position, movingRobot, robots))
                IsTrue(movingRobot.Position, nearest.Position);
            nearest.Position.X -= 1;
            if (IsCellFree(nearest.Position, movingRobot, robots))
                IsTrue(movingRobot.Position, nearest.Position);
            nearest.Position.X += 1;
            if (IsCellFree(nearest.Position, movingRobot, robots))
                IsTrue(movingRobot.Position, nearest.Position);
            nearest.Position.X += 1;
            if (IsCellFree(nearest.Position, movingRobot, robots))
                IsTrue(movingRobot.Position, nearest.Position);
            nearest.Position.Y += 1;
            if (IsCellFree(nearest.Position, movingRobot, robots))
                IsTrue(movingRobot.Position, nearest.Position);

        }

        void IsTrue(Position movingRobot, Position station)
        {
            Assert.IsFalse(movingRobot == station);
        }

        bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }
    }
}
