﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace ShabahaVladyslav.RobotChallenge.Test
{
    [TestClass]
    public class TestCountMyRobots
    {
        [TestMethod]
        public void TestMethod1()
        {
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(11, 11), Owner = new Owner(){Name = "Shabaha V."}}
            };
            int count = 0;
            foreach (var robot in robots)
            {
                if (robot.Owner.Name == "Shabaha V.")
                    count++;
            }
            Assert.AreEqual(1, count);
        }
    }
}
