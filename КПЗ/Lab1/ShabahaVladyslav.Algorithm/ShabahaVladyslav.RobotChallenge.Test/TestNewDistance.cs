﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace ShabahaVladyslav.RobotChallenge.Test
{
    [TestClass]
    public class TestNewDistance
    {
        [TestMethod]
        public void TestMethod1()
        {
            var map = new Map();
            var stationPosition = new Position(11, 11);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var nearest = map.Stations.First();
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Energy = 200, Position = new Position(1, 1)}
            };
            var movingRobot = robots.First();
            var newPosition = NewDistance(movingRobot.Position, nearest.Position, 50);
            Assert.AreEqual(50, FindDistance(newPosition, nearest.Position));
        }

        private Position NewDistance(Position a, Position b, int c)
        {
            var tmp = new Position
            {
                X = (a.X + b.X) / 2,
                Y = (a.Y + b.Y) / 2
            };
            while (FindDistance(tmp, b) > c)
            {
                tmp.X = (tmp.X + b.X) / 2;
                tmp.Y = (tmp.Y + b.Y) / 2;
            }
            return tmp;
        }

        private int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
    }
}
