﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using ShabahaVladyslav.Algorithm;

namespace ShabahaVladyslav.RobotChallenge.Test
{
    [TestClass]
    public class TestDistanceHelper
    {
        [TestMethod]
        public void TestMethod1()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(2, 4);
            Assert.AreEqual(10, DistanceHelper.FindDistance(p1, p2));
        }
    }
}
